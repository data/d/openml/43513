# OpenML dataset: Google-Stock-10Year-data2004-2020

https://www.openml.org/d/43513

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Google, one of the greatest gifts to mankind. Any information that you need today is available on Google. Google is a household name and literally everyone is aware of what google is. It helps you get resources for your school projects, helps you shop online and much more. Google has made getting education a lot easier for people across the globe. No matter where you are, you can access google provided you have internet. Every piece of info is available on google and its all one click away. But Google has a parent company known as Alphabet Inc. that trades and here we have stock data fro Alphabet Inc.
Content
This data set has 7 columns with all the necessary values such as opening price of the stock, the closing price of it, its highest in the day and much more. It has date wise data of the stock starting from 2004 to 2020(September).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43513) of an [OpenML dataset](https://www.openml.org/d/43513). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43513/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43513/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43513/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

